# Exercici HTML, CSS, SCSS 2AVA

Projecte educatiu que intenta utilitzar tot allò que s'ha donat al llarg del curs 2n DAW a l'institut IES l'Estació.

## Propietats utilitzades

### CSS
´´´
* Variables CSS
* Comentaris
* @Print
* Etiquetes <meta>
* Prefix-free
* Expressions Regulars
* :target
* Herència (:nth-child)
* Box-sizing
* Box-shadow
* Múltiples Imatges de fons
* Background-clip
* Distintes fonts
* Tranformacions
* Animacions
* Clip-path
* Imatges de lliure llicença
* Gràfic vectorial
* Atributs per a l'acessibilitat
* Patró de disseny
* Taules resposives
* Media Queries
´´´

### SCSS
´´´
* Variables
* Anidament
* Media Queries
* Sprites
* Gradient
* Filtres
* Transformacions
* Directives @if
* Depuració d'errors @error @debug
´´´

Add additional notes about how to deploy this on a live system

## Built With

* [HTML]
* [CSS]
* [SCSS]


## Authors

* **Hibernon Puig Soler** - *Initial work* - [PurpleBooth](https://gitlab.com/PuigSolerH)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

